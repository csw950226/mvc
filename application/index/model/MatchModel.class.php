<?php
class MatchModel extends Model{
    public function getlist(){
//获得比赛列表数据
        $sql = "select t1.t_name as t1_name, m.t1_score, m.t2_score, t2.t_name as t2_name, m.m_time from `match` as m left join `team` as t1 ON m.t1_id = t1.t_id  left join `team` as t2 ON m.t2_id=t2.t_id;";
        $match_list = $this->_dao->getAll($sql);
        return $match_list;
    }
}