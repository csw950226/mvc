<?php
class AdminController extends Controller {
    public function login(){
        require CURRENT_VIEW_PATH.'login.html';
    }
    public function check(){
        $name=$_POST['username'];
        $password=$_POST['password'];
        //数据库验证管理员信息
        $m_admin=Factory::M('AdminModel');
        if ($m_admin->check($name,$password)){
            //验证通过
            session_start();
            $_SESSION['login']='yes';
            $this->redirect('index.php?p=admin&c=ManageController&a=index');
        }else{
            //验证失败
            $this->_jump('index.php?p=admin&c=AdminController&a=login','密码错误',3);
        }
    }
}