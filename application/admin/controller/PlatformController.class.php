<?php
/**
 * Created by PhpStorm.
 * User: 技术员专用
 * Date: 2019/2/13
 * Time: 16:19
 */
class PlatformController extends Controller{
    public function __construct()
    {
        parent::__construct();
        $this->_checkLogin();
    }
    protected function _checkLogin(){
        session_start();
        //列出不需要登录验证的动过列表
        $no_list=[
            //控制器名=>不需要验证的动作名列表
            'Admin'=>['login','check'],
        ];
        if (isset($no_list[CONTROLLER]) && in_array(ACTION,$no_list[CONTROLLER])){
            //不需要验证
            return;
        }
        if (!isset($_SESSION['login'])){
            $this->redirect('index.php?p=admin&c=AdminController&a=login');
        }
    }
}