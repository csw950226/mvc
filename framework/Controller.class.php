<?php
class Controller{
    public function __construct()
    {
        $this->initHeader();
    }

    protected function initHeader(){
        header('Content-Type: text/html; charset=utf-8');
    }

    /**
     * 跳转
     * @param $url  URL
     * @param null $info    提示信息
     * @param int $wait 等待时间
     */
    protected function _jump($url,$info=null,$wait=3){
        header("Refresh:$wait; URL=$url");
        echo $info;
        exit;
    }

    protected function redirect($url){
        header("Location:$url");exit;
    }
}