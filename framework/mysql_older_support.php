<?php

function mysql_connect($server, $username = '', $password = '')
{
	list ($host, $port) = explode(':', $server);
	return $GLOBALS['__MYSQL_LINK__'] = mysqli_connect($host, $username, $password, null, $port);
}

function mysql_select_db($database, $link = null)
{
	return mysqli_select_db($link ?: $GLOBALS['__MYSQL_LINK__'], $database);
}

function mysql_query($sql, $link = null)
{
	return mysqli_query($link ?: $GLOBALS['__MYSQL_LINK__'], $sql);
}

function mysql_close($link = null)
{
	return mysqli_close($link ?: $GLOBALS['__MYSQL_LINK__']);
}

function mysql_error($link = null)
{
	return mysqli_error($link ?: $GLOBALS['__MYSQL_LINK__']);
}

function mysql_errno($link = null)
{
	return mysqli_errno($link ?: $GLOBALS['__MYSQL_LINK__']);
}

function mysql_num_rows($result)
{
	return mysqli_num_rows($result);
}

function mysql_real_escape_string($unescaped_string, $link = null)
{
	return mysqli_real_escape_string($link ?: $GLOBALS['__MYSQL_LINK__'], $unescaped_string);
}

function mysql_free_result($result)
{
	return mysqli_free_result($result);
}

function mysql_insert_id($link = null)
{
	return mysqli_insert_id($link ?: $GLOBALS['__MYSQL_LINK__']);
}

function mysql_fetch_assoc($result)
{
	return mysqli_fetch_assoc($result);
}

function mysql_data_seek($result, $offset)
{
	return mysqli_data_seek($result, $offset);
}

function mysql_affected_rows($link)
{
	return mysqli_affected_rows($link ?: $GLOBALS['__MYSQL_LINK__']);
}