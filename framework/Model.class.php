<?php
class Model{
    protected $_dao;
    /**
     * 构造方法
     */
    public function __construct() {
        // 初始化DAO
        $this->_initDAO();
    }
    protected function _initDao(){
        $config = array(
            'host' => $GLOBALS['config']['db']['port'],
            'port' =>$GLOBALS['config']['db']['port'],
            'username'=>$GLOBALS['config']['db']['username'],
            'password' => $GLOBALS['config']['db']['password'],
            'charset'=>$GLOBALS['config']['db']['charset'],
            'dbname'=>$GLOBALS['config']['db']['dbname']
        );
//        require_once 'MySQLDB.class.php';
        $this->_dao = MySQLDB::getInstance($config);//$dao , Database Access Object 数据库操作对象（dao层）
    }
}